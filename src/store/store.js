import Vue from "vue";
import Vuex from "vuex";
import db from "../firebase/firebaseinit";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    propiedades: null
  },
  getters: {
    getProps: state => {
      return state.propiedades
    },
    getPropById: state => (id) => {
      const getData = state.propiedades.find( data => data.prop_id == id)
      return getData
    },
    getLastProps: state => {
      return state.propiedades.slice(0).slice(-5)
    },
    getResults: (state) => (transaccion, tipo, sector) => {
      let results = []

      sector.forEach(sect => {
        let sectSec = { name: sect.name, resultados:[]}
        state.propiedades.forEach(prop => {
          if (sect.name == prop.sector) {
            transaccion.forEach(transac => {
              if (prop.transaccion == transac.name) {
                tipo.forEach(tip => {
                  if (prop.tipo == tip.name) {
                    sectSec.resultados.push(prop)
                  }
                });
              }
            });
          }
        });
        results.push(sectSec)
      });

      return results
    }
  },
  mutations: {
    setProps: state => {
      let props = []

      db.collection('propiedades').orderBy('prop_id').onSnapshot((snapshot) => {
        props = []
        snapshot.forEach((doc) => {
          props.push({
            id: doc.id,
            prop_id: doc.data().prop_id,
            valor: doc.data().valor,
            tipo: doc.data().tipo,
            transaccion: doc.data().transaccion,
            sector: doc.data().sector,
            direccion: doc.data().direccion,
            dormitorios: doc.data().dormitorios,
            baños: doc.data().baños,
            pisos: doc.data().pisos,
            metrosConstruidos: doc.data().metrosConstruidos,
            metrosCuadrados: doc.data().metrosCuadrados,
            imagen1: doc.data().imagen1,
            imagen2: doc.data().imagen2,
            imagen3: doc.data().imagen3,
            estado: doc.data().estado,
          })
        })

        state.propiedades = props
      })
    },
  },
  actions: {
    setProps: context => {
      context.commit('setProps')
    },
  }
});
